import React from 'react';
import { shallow } from 'enzyme';

import RamalItem from './RamalItem';

describe('Testing App Component', () => {
  it('shoult renders without crashing', () => {
    const wrapper = shallow(
      <RamalItem
        id={10}
        description="tes"
      />,
    );
    expect(wrapper).toMatchSnapshot();
    wrapper.setProps({ id: 1, description: 'test' });
    expect(wrapper).toMatchSnapshot();
  });
});
