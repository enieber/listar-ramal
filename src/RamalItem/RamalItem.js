import React from 'react';
import PropTypes from 'prop-types';

class RamalItem extends React.PureComponent {
  render() {
    const {
      id,
      description,
    } = this.props;
    return (
      <div
        style={{
          flexDirection: 'column',
          backgroundColor: '#fa824c',
          margin: '2px',
          padding: '2vw',
          width: '20vw',
        }}
      >
        <p
          style={{
            fontWeight: 'bold',
          }}
        >
          {`Ramal: ${id}`}
        </p>
        <p>{` ${description}`}</p>
      </div>
    );
  }
}

RamalItem.propTypes = {
  id: PropTypes.number.isRequired,
  description: PropTypes.string.isRequired,
};

export default RamalItem;
