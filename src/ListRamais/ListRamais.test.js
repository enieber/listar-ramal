import React from 'react';
import { shallow } from 'enzyme';

import ListRamais from './ListRamais';

describe('Testing ListRamais Component', () => {
  it('shoult renders without crashing', () => {
    const ramais = [
      {
        id: 1,
        description: 'test 1',
      }, {
        id: 2,
        description: 'test 2',
      },
    ];

    const wrapper = shallow(
      <ListRamais
        ramais={ramais}
      />,
    );
    expect(wrapper).toMatchSnapshot();
    wrapper.setProps({ search: '1' });
    expect(wrapper).toMatchSnapshot();
    wrapper.setProps({ search: '3' });
    expect(wrapper).toMatchSnapshot();
  });
  it('shoult renders image when no has ramal', () => {
    const ramais = [
      {
        id: 1,
        description: 'test 1',
      }, {
        id: 2,
        description: 'test 2',
      },
    ];

    const wrapper = shallow(
      <ListRamais
        ramais={ramais}
      />,
    );
    expect(wrapper).toMatchSnapshot();
    wrapper.setProps({ search: '1' });
    expect(wrapper).toMatchSnapshot();
    wrapper.setProps({ search: '-3' });
    expect(wrapper.find('img')).toHaveLength(1);
  });
});
