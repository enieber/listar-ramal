import React from 'react';
import PropTypes from 'prop-types';

import RamalItem from '../RamalItem';
import imageEmpty from '../empty.png';

class ListRamais extends React.PureComponent {
  render() {
    const {
      search,
      ramais,
    } = this.props;
    const searchText = `${search || ''}`;
    const searchList = ramais.filter(item => item.description.toLowerCase().includes(searchText.toLowerCase()) || `${item.id}`.includes(search));
    return (
      <div
        style={{
          display: 'flex',
          flexWrap: 'wrap',
          backgroundColor: '#fafffd',
          justifyContent: 'center',
        }}
      >
        {ramais.map(item => (
          <div
            key={item.id}
            style={{
              display: `${item.description.toLowerCase().includes(searchText.toLowerCase()) || `${item.id}`.includes(search) ? 'flex' : 'none'}`,
            }}
          >
            <RamalItem
              {...item}
            />
          </div>
        ))}
        {searchList.length > 0 ? (<div />) : (
          <div
            style={{
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
              flexDirection: 'column',
              margin: '5px',
              padding: '10px',
              width: '30vw',
            }}
          >
            <img
              src={imageEmpty}
              style={{
                maxHeight: '10em',
              }}
              alt="logo"
            />
            <h3
              style={{
                color: '#fa824c',
              }}
            >
Nenhum Ramal encontrado
            </h3>
          </div>
        )}
      </div>
    );
  }
}

ListRamais.propTypes = {
  ramais: PropTypes.array.isRequired,
  search: PropTypes.string,
};

ListRamais.defaultProps = {
  search: '',
};

export default ListRamais;
