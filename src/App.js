import React, { Component } from 'react';

import ListRamais from './ListRamais';
import logo from './list-ramais.png';
import './App.css';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      search: '',
      data: [],
      company: '',
    };
  }

  async componentWillMount() {
    const BASE_URL = 'https://api-ramal.herokuapp.com';
    const responseRamais = await fetch(`${BASE_URL}/ramais`);
    const dataJson = await responseRamais.json();

    const responseCompany = await fetch(`${BASE_URL}/company`);
    const dataJsonCompany = await responseCompany.json();

    this.setState({
      data: dataJson,
      company: dataJsonCompany[0],
    });
  }

  render() {
    const {
      search,
      data,
      company,
    } = this.state;
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">{`Ramais da ${company}`}</h1>
        </header>
        <main className="App-main">
          <div className="App-intro">
            <h2
              style={{
                color: '#fa824c',
                paddingRight: '1em',
                paddingLeft: '1em',
              }}
            >
              Pesquisar:
            </h2>
            <input
              style={{
                fontSize: '1.5rem',
                color: '#fa824c',
                border: 'none',
                padding: '10px 0',
                borderBottom: 'solid 1px #fa824c',
                transition: 'all 0.3s cubic-bezier(.64,.09,.08,1)',
                background: 'linear-gradient(to bottom, rgba(255,255,255,0) 96%, #fa824c 4%)',
                backgroundPosition: '-200px 0',
                backgroundSize: '200px 100%',
                backgroundRepeat: 'no-repeat',
              }}
              placeholder="Nome ou Numero"
              value={search}
              onChange={(event) => {
                this.setState({
                  search: event.target.value,
                });
              }}
            />
          </div>
          <ListRamais
            ramais={data}
            search={search}
          />
        </main>
        <footer className="App-footer">
          <h3
            style={{
              color: '#fff',
            }}
          >
            {`Todos os Direitos Reservados a ${company}`}
          </h3>
        </footer>
      </div>
    );
  }
}

export default App;
