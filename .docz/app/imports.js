export const imports = {
  'src/ListRamais/ListRamais.mdx': () =>
    import(/* webpackPrefetch: true, webpackChunkName: "src-list-ramais-list-ramais" */ 'src/ListRamais/ListRamais.mdx'),
  'src/RamalItem/RamalItem.mdx': () =>
    import(/* webpackPrefetch: true, webpackChunkName: "src-ramal-item-ramal-item" */ 'src/RamalItem/RamalItem.mdx'),
}
